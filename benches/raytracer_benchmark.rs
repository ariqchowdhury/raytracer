use crate::ray::gen_random_vec3;
use criterion::{criterion_group, criterion_main, Criterion};

#[path = "../src/color.rs"]
mod color;
#[path = "../src/hittable.rs"]
mod hittable;
#[path = "../src/ray.rs"]
mod ray;

pub fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("gen_random_vec3", |b| b.iter(|| gen_random_vec3()));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
