use ultraviolet::Vec2;

type Point = Vec2;
type Float = f32;

pub trait Sampler {
    // start sampling on the given pixel
    fn start_pixel(&mut self, p: Point);

    // get the next n sample on current pixel.
    // will return false when n == samples_per_pixel
    fn start_next_sample(&mut self) -> bool;

    // get next dimension(s) in the current sample vector
    fn get_1d(&mut self) -> Float;
    fn get_2d(&mut self) -> Point;

    // given pixel coordinates, get x, y offset for the pixel and u, v values to send to camera
    // lens offset
    fn get_camera_sample(&mut self, p: Point) -> (Point, Point) {
        let xy = p + self.get_2d();
        let lens = self.get_2d();

        (xy, lens)
    }

    fn request_1d_array(&mut self, n: u64);
    fn request_2d_array(&mut self, n: u64);

    fn get_1d_array(&mut self, n: u64) -> Option<Float>;
    fn get_2d_array(&mut self, n: u64) -> Option<Point>;

    fn get_index_for_sample(&mut self, sample: usize) -> usize;
    fn sample_dimension(&mut self, idx: usize, dim: u32) -> Float;
}

pub struct Halton {
    pub samples_per_pixel: usize,
    pub current_pixel: Point,
    current_pixel_index: usize,
    dimension: u32,
    interval_sample_index: usize,
    array_start_dim: u32,
    array_end_dim: u32,
    samples_1d_array_sizes: Vec<u64>,
    samples_2d_array_sizes: Vec<u64>,
    sample_array_1d: Vec<Vec<Float>>,
    sample_array_2d: Vec<Vec<Point>>,
    array_1d_offset: usize,
    array_2d_offset: usize,
}

impl Halton {
    pub fn new(s: usize) -> Halton {
        Halton {
            samples_per_pixel: s,
            current_pixel: Point::zero(),
            current_pixel_index: 0,
            dimension: 0,
            interval_sample_index: 0,
            array_start_dim: 5,
            array_end_dim: 5,
            samples_1d_array_sizes: Vec::new(),
            samples_2d_array_sizes: Vec::new(),
            sample_array_1d: Vec::new(),
            sample_array_2d: Vec::new(),
            array_1d_offset: 0,
            array_2d_offset: 0,
        }
    }
}

impl Sampler for Halton {
    fn start_pixel(&mut self, p: Point) {
        self.current_pixel = p;
        self.current_pixel_index = 0;
        self.dimension = 0;
        self.array_1d_offset = 0;
        self.array_2d_offset = 0;

        self.interval_sample_index = self.get_index_for_sample(0);
        self.array_end_dim = self.array_start_dim
            + self.sample_array_1d.len() as u32
            + 2 * self.sample_array_2d.len() as u32;

        for i in 0..self.samples_1d_array_sizes.len() {
            let n_samples = self.samples_1d_array_sizes[i] as usize * self.samples_per_pixel;
            for j in 0..n_samples {
                let idx = self.get_index_for_sample(j);
                self.sample_array_1d[i][j] =
                    self.sample_dimension(idx, self.array_start_dim + i as u32);
            }
        }

        let mut dim = self.array_start_dim + self.samples_1d_array_sizes.len() as u32;
        for i in 0..self.samples_2d_array_sizes.len() {
            let n_samples = self.samples_2d_array_sizes[i] as usize * self.samples_per_pixel;
            for j in 0..n_samples as usize {
                let idx = self.get_index_for_sample(j);
                self.sample_array_2d[i][j].x = self.sample_dimension(idx, dim);
                self.sample_array_2d[i][j].y = self.sample_dimension(idx, dim + 1);
            }
            dim += 2;
        }

        assert_eq!(self.array_end_dim, dim);
    }

    fn start_next_sample(&mut self) -> bool {
        self.dimension = 0;
        self.interval_sample_index = self.get_index_for_sample(self.current_pixel_index + 1);
        self.array_1d_offset = 0;
        self.array_2d_offset = 0;
        self.current_pixel_index += 1;
        self.current_pixel_index < self.samples_per_pixel
    }

    fn request_1d_array(&mut self, n: u64) {
        self.samples_1d_array_sizes.push(n);
        self.sample_array_1d.push(Vec::<Float>::with_capacity(
            n as usize * self.samples_per_pixel,
        ));
    }

    fn request_2d_array(&mut self, n: u64) {
        self.samples_2d_array_sizes.push(n);
        self.sample_array_2d.push(Vec::<Point>::with_capacity(
            n as usize * self.samples_per_pixel,
        ));
    }

    fn get_1d_array(&mut self, n: u64) -> Option<Float> {
        if self.array_1d_offset == self.sample_array_1d.len() {
            return None;
        }

        let r =
            Some(self.sample_array_1d[self.array_1d_offset][self.current_pixel_index * n as usize]);
        self.array_1d_offset += 1;
        return r;
    }

    fn get_2d_array(&mut self, n: u64) -> Option<Point> {
        if self.array_2d_offset == self.sample_array_2d.len() {
            return None;
        }

        let r =
            Some(self.sample_array_2d[self.array_2d_offset][self.current_pixel_index * n as usize]);
        self.array_2d_offset += 1;
        return r;
    }

    fn get_1d(&mut self) -> Float {
        if self.dimension >= self.array_start_dim && self.dimension < self.array_end_dim {
            self.dimension = self.array_end_dim;
        }
        let r = self.sample_dimension(self.interval_sample_index, self.dimension);
        self.dimension += 1;
        return r;
    }

    fn get_2d(&mut self) -> Point {
        if self.dimension + 1 >= self.array_start_dim && self.dimension < self.array_end_dim {
            self.dimension = self.array_end_dim;
        }
        let r = Point::new(
            self.sample_dimension(self.interval_sample_index, self.dimension),
            self.sample_dimension(self.interval_sample_index, self.dimension + 1),
        );
        self.dimension += 2;
        return r;
    }

    fn get_index_for_sample(&mut self, sample: usize) -> usize {
        0
    }

    fn sample_dimension(&mut self, idx: usize, dim: u32) -> Float {
        0.0
    }
}

pub fn radical_inverse(base_idx: u64, a: u64) -> Float {
    match base_idx {
        0 => radical_inverse_impl::<2>(a),
        1 => radical_inverse_impl::<3>(a),
        2 => radical_inverse_impl::<5>(a),
        3 => radical_inverse_impl::<7>(a),
        4 => radical_inverse_impl::<11>(a),
        5 => radical_inverse_impl::<13>(a),
        6 => radical_inverse_impl::<17>(a),
        7 => radical_inverse_impl::<19>(a),
        8 => radical_inverse_impl::<23>(a),
        9 => radical_inverse_impl::<29>(a),
        10 => radical_inverse_impl::<31>(a),
        11 => radical_inverse_impl::<37>(a),
        12 => radical_inverse_impl::<39>(a),
        _ => {
            assert!(false);
            0.0
        }
    }
}

fn radical_inverse_impl<const BASE: u64>(i: u64) -> Float {
    let inv_base = 1.0 / BASE as Float;
    let mut reversed_digits: u64 = 0;
    let mut inv_base_n: Float = 1.0;

    let mut a = i;
    while a != 0 {
        let next = a / BASE;
        let digit = a - next * BASE;
        reversed_digits = reversed_digits * BASE + digit;
        inv_base_n *= inv_base;
        a = next
    }

    return (reversed_digits as Float * inv_base_n).min(1.0 - Float::EPSILON);
}

fn inverse_radical_inverse<const BASE: u64>(inverse: u64, n_digits: u32) -> u64 {
    let mut index: u64 = 0;
    let mut i = inverse;
    for _ in 0..n_digits {
        let digit = inverse % BASE;
        i /= BASE;
        index = index * BASE + digit;
    }
    index
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_halton() {
        let width = 10;
        let height = 10;

        let total = width * height;

        let mut k = Vec::<(f32, f32)>::with_capacity(total);

        for n in (0 + 1400..2 * total + 1400).step_by(2) {
            k.push((
                radical_inverse(2, n as u64),
                radical_inverse(2, n as u64 + 1),
            ));
        }

        for (i, (x, y)) in k.iter().enumerate() {
            let u = (*x * width as f32).floor() as u32;
            let v = (*y * height as f32).floor() as u32;
        }
    }
}
