use ultraviolet::Vec3;

#[inline]
pub fn face_forward(normal: Vec3, v: Vec3) -> Vec3 {
    if v.dot(normal) < 0.0 {
        normal
    } else {
        -normal
    }
}
