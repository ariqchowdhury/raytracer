use crate::bounds::Bounds;
use crate::color::Color;
use crate::geometry::face_forward;
use crate::hittable::{IntersectionEvent, IntersectionGeometry};
use crate::materials::Material;
use crate::ray::Ray;
use crate::shapes::{Interaction, Shape};
use crate::transform::Transform;
use crate::util;
use std::sync::Arc;
use ultraviolet::{Vec2, Vec3};

pub struct Disk {
    pub radius: f32,
    pub inner_radius: f32,
    pub height: f32,
    // for a full disk, wil be 2pi
    pub phi_max: f32,

    pub obj_to_world: Arc<Transform>,
    pub world_to_obj: Arc<Transform>,

    pub material: Box<dyn Material>,
}

impl Disk {
    pub fn new(
        o2w: Arc<Transform>,
        w2o: Arc<Transform>,
        radius: f32,
        inner_radius: f32,
        height: f32,
        phi_max: f32,
        material: Box<dyn Material>,
    ) -> Disk {
        Disk {
            radius: radius,
            inner_radius: inner_radius,
            height: height,
            phi_max: f32::to_radians(util::clamp(phi_max, 0.0, 360.0)),

            obj_to_world: o2w,
            world_to_obj: w2o,
            material: material,
        }
    }
}

impl Shape for Disk {
    fn object_bound(&self) -> Bounds {
        Bounds {
            p_min: Vec3::new(-self.radius, -self.radius, self.height),
            p_max: Vec3::new(self.radius, self.radius, self.height),
        }
    }

    fn world_bound(&self) -> Bounds {
        self.obj_to_world.bound_mul(&self.object_bound())
    }

    fn object_to_world(&self) -> Arc<Transform> {
        return self.obj_to_world.clone();
    }

    fn world_to_object(&self) -> Arc<Transform> {
        return self.world_to_obj.clone();
    }

    fn area(&self) -> f32 {
        self.phi_max * 0.5 * (self.radius * self.radius - self.inner_radius * self.inner_radius)
    }

    fn pdf(&self) -> f32 {
        self.area().recip()
    }
}

impl Interaction for Disk {
    fn intersect(&self, r: &Ray, t_max: f32) -> (IntersectionEvent, IntersectionGeometry) {
        let mut event = IntersectionEvent::new();
        let ray = self.world_to_obj.ray_mul(r);

        if ray.direction.z == 0.0 {
            event.t = -1.0;
            return (event, IntersectionGeometry::empty());
        }

        let t_shape_hit = (self.height - ray.origin.z) / ray.direction.z;
        if t_shape_hit <= 0.001 || t_shape_hit >= t_max {
            event.t = -1.0;
            return (event, IntersectionGeometry::empty());
        }

        let p_hit = ray.at(t_shape_hit);
        let dist2 = p_hit.x * p_hit.x + p_hit.y * p_hit.y;
        if dist2 > (self.radius * self.radius) || dist2 < (self.inner_radius * self.inner_radius) {
            event.t = -1.0;
            return (event, IntersectionGeometry::empty());
        }

        let mut phi = p_hit.y.atan2(p_hit.x);
        if phi < 0.0 {
            phi += 2.0 * std::f32::consts::PI;
        }
        if phi > self.phi_max {
            event.t = -1.0;
            return (event, IntersectionGeometry::empty());
        }

        let u = phi / self.phi_max;
        let r_hit = dist2.sqrt();
        let v = (self.radius - r_hit) / (self.radius - self.inner_radius);

        let dpdu = Vec3::new(-self.phi_max * p_hit.y, self.phi_max * p_hit.x, 0.0);
        let dpdv = Vec3::new(p_hit.x, p_hit.y, 0.0) * (self.inner_radius - self.radius) / r_hit;

        let dndu = Vec3::zero();
        let dndv = Vec3::zero();

        let intersection = IntersectionGeometry::new(
            p_hit,
            Vec3::zero(),
            -ray.direction,
            Vec2::new(u, v),
            dpdu,
            dpdv,
            dndu,
            dndv,
        );

        event.t = t_shape_hit;
        event.p = self.obj_to_world.pt_mul3(&p_hit);

        event.normal = face_forward(self.obj_to_world.norm_mul(&intersection.n), ray.direction);
        return (event, intersection);
    }

    fn intersect_p(&self) -> bool {
        false
    }

    fn material_scatter(
        &self,
        r_in: &Ray,
        event: &IntersectionEvent,
        i_g: &IntersectionGeometry,
    ) -> (bool, Color, Ray) {
        self.material.scatter(r_in, event, i_g)
    }

    fn emit_light(&self, i_g: &IntersectionGeometry) -> Color {
        self.material.emit_light(i_g)
    }
}
