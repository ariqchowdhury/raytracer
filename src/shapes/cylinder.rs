use crate::bounds::Bounds;
use crate::color::Color;
use crate::geometry::face_forward;
use crate::hittable::{IntersectionEvent, IntersectionGeometry};
use crate::materials::Material;
use crate::ray::Ray;
use crate::shapes::{Interaction, Shape};
use crate::transform::Transform;
use crate::util;
use std::sync::Arc;
use ultraviolet::{Vec2, Vec3};

pub struct Cylinder {
    pub radius: f32,
    pub z_min: f32,
    pub z_max: f32,
    // for a full sphere, wil be 2pi
    pub phi_max: f32,

    pub obj_to_world: Arc<Transform>,
    pub world_to_obj: Arc<Transform>,

    pub material: Box<dyn Material>,
}

impl Cylinder {
    pub fn new(
        o2w: Arc<Transform>,
        w2o: Arc<Transform>,
        radius: f32,
        z_min: f32,
        z_max: f32,
        phi_max: f32,
        material: Box<dyn Material>,
    ) -> Cylinder {
        Cylinder {
            radius: radius,
            z_min: f32::min(z_min, z_max),
            z_max: f32::max(z_min, z_max),
            phi_max: f32::to_radians(util::clamp(phi_max, 0.0, 360.0)),

            obj_to_world: o2w,
            world_to_obj: w2o,
            material: material,
        }
    }
}

impl Shape for Cylinder {
    fn object_bound(&self) -> Bounds {
        Bounds {
            p_min: Vec3::new(-self.radius, -self.radius, self.z_min),
            p_max: Vec3::new(self.radius, self.radius, self.z_max),
        }
    }

    fn world_bound(&self) -> Bounds {
        self.obj_to_world.bound_mul(&self.object_bound())
    }

    fn object_to_world(&self) -> Arc<Transform> {
        return self.obj_to_world.clone();
    }

    fn world_to_object(&self) -> Arc<Transform> {
        return self.world_to_obj.clone();
    }

    fn area(&self) -> f32 {
        self.phi_max * self.radius * (self.z_max - self.z_min)
    }

    fn pdf(&self) -> f32 {
        self.area().recip()
    }
}

impl Interaction for Cylinder {
    fn intersect(&self, r: &Ray, t_max: f32) -> (IntersectionEvent, IntersectionGeometry) {
        let mut event = IntersectionEvent::new();
        let ray = self.world_to_obj.ray_mul(r);

        let a = ray.direction.x * ray.direction.x + ray.direction.y * ray.direction.y;
        let b = ray.direction.x * ray.origin.x + ray.direction.y * ray.origin.y;
        let c =
            ray.origin.x * ray.origin.x + ray.origin.y * ray.origin.y - self.radius * self.radius;

        let d = b * b - a * c;
        if d < 0.0 {
            event.t = -1.0;
            return (event, IntersectionGeometry::empty());
        }

        let sqrt_d = d.sqrt();
        let mut t0 = (-b - sqrt_d) / a;
        let mut t1 = (-b + sqrt_d) / a;
        if t0 > t1 {
            std::mem::swap(&mut t0, &mut t1);
        }

        if t0 > t_max || t1 <= 0.001 {
            event.t = -1.0;
            return (event, IntersectionGeometry::empty());
        }
        let mut t_shape_hit = t0;
        if t_shape_hit <= 0.001 {
            t_shape_hit = t1;
            if t_shape_hit > t_max {
                event.t = -1.0;
                return (event, IntersectionGeometry::empty());
            }
        }

        let mut p_hit = ray.at(t_shape_hit);

        let hit_rad = (p_hit.x * p_hit.x + p_hit.y * p_hit.y).sqrt();
        p_hit.x *= self.radius / hit_rad;
        p_hit.y *= self.radius / hit_rad;

        let mut phi = p_hit.y.atan2(p_hit.x);
        if phi < 0.0 {
            phi += 2.0 * std::f32::consts::PI;
        }

        // deal with clipped z or phi ranges
        if p_hit.z < self.z_min || p_hit.z > self.z_max || phi > self.phi_max {
            if t_shape_hit == t1 {
                event.t = -1.0;
                return (event, IntersectionGeometry::empty());
            }
            t_shape_hit = t1;
            if t1 > t_max {
                event.t = -1.0;
                return (event, IntersectionGeometry::empty());
            }
            p_hit = ray.at(t_shape_hit);
            let hit_rad = (p_hit.x * p_hit.x + p_hit.y * p_hit.y).sqrt();
            p_hit.x *= self.radius / hit_rad;
            p_hit.y *= self.radius / hit_rad;

            phi = p_hit.y.atan2(p_hit.x);
            if phi < 0.0 {
                phi += 2.0 * std::f32::consts::PI;
            }

            if p_hit.z < self.z_min || p_hit.z > self.z_max || phi > self.phi_max {
                event.t = -1.0;
                return (event, IntersectionGeometry::empty());
            }
        }

        //calculations for interaction geometry (normal, etc)
        let u = phi / self.phi_max;
        let v = (p_hit.z - self.z_min) / (self.z_max - self.z_min);

        let dpdu = Vec3::new(-self.phi_max * p_hit.y, self.phi_max * p_hit.x, 0.0);
        let dpdv = Vec3::new(0.0, 0.0, self.z_max - self.z_min);

        let dndu = Vec3::zero();
        let dndv = Vec3::zero();

        let intersection = IntersectionGeometry::new(
            p_hit,
            Vec3::zero(),
            -ray.direction,
            Vec2::new(u, v),
            dpdu,
            dpdv,
            dndu,
            dndv,
        );

        event.t = t_shape_hit;
        event.p = self.obj_to_world.pt_mul3(&p_hit);

        event.normal = face_forward(self.obj_to_world.norm_mul(&intersection.n), ray.direction);
        return (event, intersection);
    }

    fn intersect_p(&self) -> bool {
        false
    }

    fn material_scatter(
        &self,
        r_in: &Ray,
        event: &IntersectionEvent,
        i_g: &IntersectionGeometry,
    ) -> (bool, Color, Ray) {
        self.material.scatter(r_in, event, i_g)
    }

    fn emit_light(&self, i_g: &IntersectionGeometry) -> Color {
        self.material.emit_light(i_g)
    }
}
