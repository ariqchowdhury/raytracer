use crate::bounds::Bounds;
use crate::color::Color;
use crate::geometry::face_forward;
use crate::hittable::{IntersectionEvent, IntersectionGeometry};
use crate::materials::Material;
use crate::ray::Ray;
use crate::shapes::{Interaction, Shape};
use crate::transform::Transform;
use crate::util;
use std::sync::Arc;
use ultraviolet::{Vec2, Vec3};

pub struct Sphere {
    pub radius: f32,
    // for a full sphere, will be -r
    pub z_min: f32,
    // for a full sphere, will be r
    pub z_max: f32,
    pub theta_min: f32,
    pub theta_max: f32,
    // for a full sphere, wil be 2pi
    pub phi_max: f32,

    pub obj_to_world: Arc<Transform>,
    pub world_to_obj: Arc<Transform>,

    pub material: Box<dyn Material>,
}

impl Sphere {
    pub fn new(
        o2w: Arc<Transform>,
        w2o: Arc<Transform>,
        radius: f32,
        z_min: f32,
        z_max: f32,
        phi_max: f32,
        material: Box<dyn Material>,
    ) -> Sphere {
        Sphere {
            radius: radius,
            z_min: util::clamp(f32::min(z_min, z_max), -radius, radius),
            z_max: util::clamp(f32::max(z_min, z_max), -radius, radius),
            theta_min: f32::acos(util::clamp(z_min / radius, -1.0, 1.0)),
            theta_max: f32::acos(util::clamp(z_max / radius, -1.0, 1.0)),
            phi_max: f32::to_radians(util::clamp(phi_max, 0.0, 360.0)),

            obj_to_world: o2w,
            world_to_obj: w2o,
            material: material,
        }
    }
}

impl Shape for Sphere {
    fn object_bound(&self) -> Bounds {
        Bounds {
            p_min: Vec3::new(-self.radius, -self.radius, self.z_min),
            p_max: Vec3::new(self.radius, self.radius, self.z_max),
        }
    }

    fn world_bound(&self) -> Bounds {
        self.obj_to_world.bound_mul(&self.object_bound())
    }

    fn object_to_world(&self) -> Arc<Transform> {
        return self.obj_to_world.clone();
    }

    fn world_to_object(&self) -> Arc<Transform> {
        return self.world_to_obj.clone();
    }

    fn area(&self) -> f32 {
        self.phi_max * self.radius * (self.z_max - self.z_min)
    }

    fn pdf(&self) -> f32 {
        self.area().recip()
    }
}

impl Interaction for Sphere {
    fn intersect(&self, r: &Ray, t_max: f32) -> (IntersectionEvent, IntersectionGeometry) {
        let mut event = IntersectionEvent::new();
        let ray = self.world_to_obj.ray_mul(r);

        let a = ray.direction.mag_sq();
        let b = ray.origin.dot(ray.direction);
        let c = ray.origin.mag_sq() - self.radius * self.radius;

        let d = b * b - a * c;
        if d < 0.0 {
            event.t = -1.0;
            return (event, IntersectionGeometry::empty());
        }

        let sqrt_d = d.sqrt();
        let mut t0 = (-b - sqrt_d) / a;
        let mut t1 = (-b + sqrt_d) / a;
        if t0 > t1 {
            std::mem::swap(&mut t0, &mut t1);
        }

        if t0 > t_max || t1 <= 0.001 {
            event.t = -1.0;
            return (event, IntersectionGeometry::empty());
        }
        let mut t_shape_hit = t0;
        if t_shape_hit <= 0.001 {
            t_shape_hit = t1;
            if t_shape_hit > t_max {
                event.t = -1.0;
                return (event, IntersectionGeometry::empty());
            }
        }

        let mut p_hit = ray.at(t_shape_hit);
        p_hit *= self.radius / (p_hit.x * p_hit.x + p_hit.y * p_hit.y + p_hit.z * p_hit.z).sqrt();
        if p_hit.x == 0.0 && p_hit.y == 0.0 {
            p_hit.x = 1e-5 * self.radius;
        }
        let mut phi = p_hit.y.atan2(p_hit.x);
        if phi < 0.0 {
            phi += 2.0 * std::f32::consts::PI;
        }

        // deal with clipped z or phi ranges
        if (self.z_min > -self.radius && p_hit.z < self.z_min)
            || (self.z_max < self.radius && p_hit.z > self.z_max)
            || phi > self.phi_max
        {
            if t_shape_hit == t1 {
                event.t = -1.0;
                return (event, IntersectionGeometry::empty());
            }
            if t1 > t_max {
                event.t = -1.0;
                return (event, IntersectionGeometry::empty());
            }
            t_shape_hit = t1;
            p_hit = ray.at(t_shape_hit);
            p_hit *=
                self.radius / (p_hit.x * p_hit.x + p_hit.y * p_hit.y + p_hit.z * p_hit.z).sqrt();
            if p_hit.x == 0.0 && p_hit.y == 0.0 {
                p_hit.x = 1e-5 * self.radius;
            }
            phi = p_hit.y.atan2(p_hit.x);
            if phi < 0.0 {
                phi += 2.0 * std::f32::consts::PI;
            }

            if (self.z_min > -self.radius && p_hit.z < self.z_min)
                || (self.z_max < self.radius && p_hit.z > self.z_max)
                || phi > self.phi_max
            {
                event.t = -1.0;
                return (event, IntersectionGeometry::empty());
            }
        }

        //calculations for interaction geometry (normal, etc)
        let u = phi / self.phi_max;
        let theta = util::clamp(p_hit.z / self.radius, -1.0, 1.0).acos();
        let v = (theta / self.theta_min) / (self.theta_max - self.theta_min);

        let zradius = (p_hit.x * p_hit.x + p_hit.y * p_hit.y).sqrt();
        let inv_zradius = 1.0 / zradius;
        let cos_phi = p_hit.x * inv_zradius;
        let sin_phi = p_hit.y * inv_zradius;

        let dpdu = Vec3::new(-self.phi_max * p_hit.y, self.phi_max * p_hit.x, 0.0);
        let dpdv = (self.theta_max - self.theta_min)
            * Vec3::new(
                p_hit.z * cos_phi,
                p_hit.z * sin_phi,
                -self.radius * theta.sin(),
            );

        //TODO: compute dndu, dndv
        let dndu = Vec3::zero();
        let dndv = Vec3::zero();

        let intersection = IntersectionGeometry::new(
            p_hit,
            Vec3::zero(),
            -ray.direction,
            Vec2::new(u, v),
            dpdu,
            dpdv,
            dndu,
            dndv,
        );

        // event is similar to intersectionGeometry but we will keep all the values in world space.
        // TODO: eventually replace event and only use the intersectionGeometry and have the
        // materials figure out the right space coordinations
        event.t = t_shape_hit;
        event.p = self.obj_to_world.pt_mul3(&p_hit);

        event.normal = face_forward(self.obj_to_world.norm_mul(&intersection.n), ray.direction);
        return (event, intersection);
    }

    fn intersect_p(&self) -> bool {
        false
    }

    fn material_scatter(
        &self,
        r_in: &Ray,
        event: &IntersectionEvent,
        i_g: &IntersectionGeometry,
    ) -> (bool, Color, Ray) {
        self.material.scatter(r_in, event, i_g)
    }

    fn emit_light(&self, i_g: &IntersectionGeometry) -> Color {
        self.material.emit_light(i_g)
    }
}
