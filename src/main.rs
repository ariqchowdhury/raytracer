use image::ImageBuffer;
use pixels::{Error, Pixels, SurfaceTexture};
use rand::Rng;
use structopt::StructOpt;
use winit::{
    event::{ElementState, Event, KeyboardInput, VirtualKeyCode, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
};

mod bounds;
mod bvh;
mod camera;
mod color;
mod geometry;
mod hittable;
mod integrator;
mod light;
mod materials;
mod ray;
mod render;
mod sampler;
mod shapes;
mod texture;
mod transform;
mod util;

use bvh::BVHAccelWorld;
use camera::BasicCamera;
use color::Color;
use integrator::{BruteForceLights, BruteForceWorld, Whitted};
use light::{GlobalInfiniteLight, LightEmission};
use materials::{DiffuseLight, Lambertian, Metal};
use render::{
    compute_color_wide, halton_render_frame, BoxFilter, GaussianFilter, MitchellFilter,
    TriangleFilter, ASPECT_RATIO, HEIGHT, WIDTH,
};
use shapes::cylinder::Cylinder;
use shapes::disk::Disk;
use shapes::sphere::Sphere;
use shapes::Shape;
use std::sync::Arc;
use texture::{Checker, SolidColor};
use transform::Transform;
use ultraviolet::{f32x4, Vec3};

#[derive(StructOpt)]
struct Cli {
    /// Number of times to sample the frame (MAX 500)
    #[structopt(short, long, default_value = "1")]
    samplesize: u32,

    /// Which scene should be loaded
    #[structopt(long, default_value = "world_one")]
    scene: String,

    #[structopt(long, default_value = "halton")]
    sampler: String,

    #[structopt(short, long)]
    file: bool,
}

fn main() -> Result<(), Error> {
    let args = Cli::from_args();
    let mut samplesize = if args.samplesize == 0 {
        1
    } else if args.samplesize > 500 {
        500
    } else {
        args.samplesize
    };

    let camera_origin_default = Vec3::new(0.0, 0.3, -2.5);
    let mut camera_origin = camera_origin_default;

    let event_loop = EventLoop::new();
    let size = winit::dpi::LogicalSize::new(WIDTH as f32, HEIGHT as f32);
    let window = WindowBuilder::new()
        .with_title("Rust Path Tracer")
        .with_inner_size(size)
        .with_min_inner_size(size)
        .with_resizable(false)
        .build(&event_loop)
        .unwrap();

    let mut pixels = {
        let window_size = window.inner_size();
        let surface_texture = SurfaceTexture::new(window_size.width, window_size.height, &window);
        Pixels::new(WIDTH, HEIGHT, surface_texture)?
    };

    // WORLD
    let mut world = BruteForceWorld {
        scene: Vec::<Arc<dyn Shape>>::new(),
    };
    let mut bvh_world = BVHAccelWorld::new(Vec::<Arc<dyn Shape>>::new(), 255);

    match &args.scene[..] {
        "world_one" => {
            world_one(&mut world.scene);
            world_one(&mut bvh_world.primitives);
        }
        "world_two" => {
            world_two(&mut world.scene);
            world_two(&mut bvh_world.primitives);
        }
        "world_three" => {
            world_three(&mut world.scene);
            world_three(&mut bvh_world.primitives);
        }
        _ => {
            world_three(&mut world.scene);
            world_three(&mut bvh_world.primitives);
        }
    }

    bvh_world.init();

    // LIGHTS
    let mut lights = BruteForceLights {
        lights: Vec::<Box<dyn LightEmission>>::new(),
    };
    lights.lights.push(Box::new(GlobalInfiniteLight {
        lerp_color_start: Color::saturated(),
        lerp_color_end: Color(Vec3::new(0.45, 0.65, 1.0)),
    }));
    /*
    lights.lights.push(Box::new(GlobalInfiniteLight {
        lerp_color_start: Color::black(),
        lerp_color_end: Color::black(),
    }));
    */

    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Wait;
        match event {
            Event::WindowEvent { event, .. } => match event {
                WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                WindowEvent::KeyboardInput {
                    input:
                        KeyboardInput {
                            virtual_keycode: Some(virtual_code),
                            state: ElementState::Released,
                            ..
                        },
                    ..
                } => match virtual_code {
                    VirtualKeyCode::Q => {
                        *control_flow = ControlFlow::Exit;
                    }
                    VirtualKeyCode::F => {
                        samplesize = 2;
                        window.request_redraw();
                    }
                    VirtualKeyCode::R => {
                        samplesize = 500;
                        window.request_redraw();
                    }
                    VirtualKeyCode::K => {
                        camera_origin += Vec3::new(0.0, -0.5, 0.0);
                        if camera_origin.y <= 0.1 {
                            camera_origin.y = 0.3;
                        }
                        window.request_redraw();
                    }
                    VirtualKeyCode::I => {
                        samplesize = 2;
                        camera_origin += Vec3::new(0.0, 0.5, 0.0);
                        window.request_redraw();
                    }
                    VirtualKeyCode::J => {
                        samplesize = 2;
                        camera_origin += Vec3::new(-1.0, 0.0, 0.0);
                        window.request_redraw();
                    }
                    VirtualKeyCode::L => {
                        samplesize = 2;
                        camera_origin += Vec3::new(1.0, 0.0, 0.0);
                        window.request_redraw();
                    }
                    VirtualKeyCode::U => {
                        samplesize = 2;
                        camera_origin += Vec3::new(0.0, 0.0, 1.0);
                        window.request_redraw();
                    }
                    VirtualKeyCode::O => {
                        samplesize = 2;
                        camera_origin += Vec3::new(0.0, 0.0, -1.0);
                        window.request_redraw();
                    }
                    VirtualKeyCode::Space => {
                        samplesize = 2;
                        camera_origin = camera_origin_default;
                        window.request_redraw();
                    }
                    _ => (),
                },
                _ => (),
            },
            Event::RedrawRequested(_) => {
                let camera = make_camera(camera_origin);
                println!("camera origin: {:?}", camera.origin);
                //let filter = BoxFilter {};
                //let filter = TriangleFilter::new(1.0);
                let filter = GaussianFilter::new(1.0, 0.2);
                //let filter = MitchellFilter::new(1.0, 2.0, -0.5);

                let frame = pixels.get_frame();
                use std::time::Instant;
                let mut now = Instant::now();

                let (f, gamma) = if args.sampler == "halton" {
                    (
                        halton_render_frame(
                            &camera,
                            Whitted { depth: 35 },
                            &bvh_world,
                            &lights,
                            samplesize,
                            &filter,
                        ),
                        1.0,
                    )
                } else {
                    (
                        halton_render_frame(
                            &camera,
                            Whitted { depth: 35 },
                            &bvh_world,
                            &lights,
                            samplesize,
                            &filter,
                        ),
                        1.0,
                    )
                };

                let elapsed = now.elapsed();
                println!("total render: {:.2?}", elapsed);

                now = Instant::now();
                for (px, data) in frame.chunks_exact_mut(4).rev().zip(f.f.chunks_exact(4)) {
                    let colors = compute_color_wide(f32x4::from(&data[0..4]), gamma);
                    px[0] = colors[0] as u8;
                    px[1] = colors[1] as u8;
                    px[2] = colors[2] as u8;
                    px[3] = 0xFF;
                }
                let elapsed = now.elapsed();
                println!("total compute color: {:.2?}", elapsed);

                if args.file {
                    let mut image = ImageBuffer::new(WIDTH, HEIGHT);
                    for (px, data) in image.pixels_mut().zip(frame.chunks_exact(4)).rev() {
                        *px = image::Rgba([data[0], data[1], data[2], data[3]]);
                    }

                    let mut file_name = String::from("images/".to_string());
                    file_name.push_str(&args.sampler.to_string());
                    file_name.push_str(&args.samplesize.to_string());
                    file_name.push_str(".png");
                    image.save(file_name).unwrap();
                }

                if pixels
                    .render()
                    .map_err(|e| println!("pixels.render() failed: {}", e))
                    .is_err()
                {
                    *control_flow = ControlFlow::Exit;
                    return;
                }
            }
            /*Event::MainEventsCleared => {
                window.request_redraw();
            }*/
            _ => (),
        }
    });
}

fn make_camera(camera_origin: Vec3) -> BasicCamera {
    let look_at = Vec3::new(0.0, 0.0, 1.0);

    BasicCamera::new(
        camera_origin,
        look_at,
        Vec3::new(0.0, 1.0, 0.0),
        30.0,
        ASPECT_RATIO,
        0.0,
        (camera_origin - look_at).mag(),
    )
}

fn world_one(world: &mut Vec<Arc<dyn Shape>>) {
    //right
    world.push(Arc::new(Sphere::new(
        Arc::new(Transform::new_translate(1.0, 0.0, 1.0)),
        Arc::new(Transform::new_translate(-1.0, 0.0, -1.0)),
        0.5,
        -0.5,
        0.5,
        360.0,
        /*
        Box::new(Metal {
            albedo: Color::new(205, 165, 60),
            fuzz: 0.05,
        }),
        */
        Box::new(Lambertian {
            texture: Arc::new(SolidColor::new(Color::new(0, 0, 200))),
        }),
    )));
    //centre
    world.push(Arc::new(Sphere::new(
        Arc::new(Transform::new_translate(0.0, 0.0, 1.0)),
        Arc::new(Transform::new_translate(0.0, 0.0, -1.0)),
        0.5,
        -0.5,
        0.5,
        360.0,
        /*
        Box::new(Lambertian {
            albedo: Color::new(0, 200, 0),
        }),
        */
        Box::new(Metal {
            albedo: Color::new(205, 205, 205),
            fuzz: 0.5,
        }),
    )));
    //left
    world.push(Arc::new(Sphere::new(
        Arc::new(Transform::new_translate(-1.0, 0.0, 1.0)),
        Arc::new(Transform::new_translate(1.0, 0.0, -1.0)),
        0.5,
        -0.5,
        0.5,
        360.0,
        Box::new(Lambertian {
            texture: Arc::new(SolidColor::new(Color::new(200, 0, 0))),
        }),
        /*
        Box::new(Metal {
            albedo: Color::new(205, 205, 205),
            fuzz: 0.5,
        }),
        */
    )));
    //ground
    world.push(Arc::new(Sphere::new(
        Arc::new(Transform::new_translate(0.0, -120.5, 1.0)),
        Arc::new(Transform::new_translate(0.0, 120.5, -1.0)),
        120.0,
        -120.0,
        120.0,
        360.0,
        Box::new(Lambertian {
            texture: Arc::new(Checker::<SolidColor>::new(
                SolidColor::new(Color::new(180, 24, 24)),
                SolidColor::new(Color::new(200, 200, 200)),
            )),
        }),
    )));
}

fn world_two(world: &mut Vec<Arc<dyn Shape>>) {
    //centre
    world.push(Arc::new(Sphere::new(
        Arc::new(Transform::new_translate(0.0, 0.0, 1.0)),
        Arc::new(Transform::new_translate(0.0, 0.0, -1.0)),
        0.5,
        -0.5,
        0.5,
        360.0,
        Box::new(Lambertian {
            texture: Arc::new(SolidColor::new(Color::new(200, 64, 64))),
        }),
    )));
    //right
    world.push(Arc::new(Sphere::new(
        Arc::new(Transform::new_translate(1.2, 0.0, 1.0)),
        Arc::new(Transform::new_translate(-1.2, 0.0, -1.0)),
        0.5,
        -0.5,
        0.5,
        360.0,
        Box::new(Metal {
            albedo: Color::new(205, 205, 205),
            fuzz: 0.0,
        }),
    )));
    //left
    world.push(Arc::new(Sphere::new(
        Arc::new(Transform::new_translate(-1.2, 0.0, 1.0)),
        Arc::new(Transform::new_translate(1.2, 0.0, -1.0)),
        0.5,
        -0.5,
        0.5,
        360.0,
        Box::new(Metal {
            albedo: Color::new(45, 137, 186),
            fuzz: 0.1,
        }),
    )));
    //ground
    world.push(Arc::new(Sphere::new(
        Arc::new(Transform::new_translate(0.0, -400.5, 1.0)),
        Arc::new(Transform::new_translate(0.0, 400.5, -1.0)),
        400.0,
        -400.0,
        400.0,
        360.0,
        Box::new(Lambertian {
            texture: Arc::new(Checker::<SolidColor>::new(
                SolidColor::new(Color::new(5, 37, 23)),
                SolidColor::new(Color::new(200, 200, 200)),
            )),
        }),
    )));

    let mut rng = rand::thread_rng();
    for _ in 0..64 {
        let x = rng.gen_range(-2.0..2.0);
        let z = rng.gen_range(-2.0..4.0);
        world.push(Arc::new(Sphere::new(
            Arc::new(Transform::new_translate(x, -0.435, z)),
            Arc::new(Transform::new_translate(-x, 0.435, -z)),
            0.075,
            -0.075,
            0.075,
            360.0,
            Box::new(DiffuseLight {
                texture: Arc::new(SolidColor::new(Color::random())),
                intensity: 1.4,
            }),
        )));
    }

    /*
    for _ in 0..64 {
        let x = rng.gen_range(-2.0..2.0);
        let z = rng.gen_range(-2.0..2.0);
        let y = rng.gen_range(-0.2..0.2);
        world.push(Arc::new(Sphere::new(
            Arc::new(Transform::new_translate(x, 0.8+y, z)),
            Arc::new(Transform::new_translate(-x, -(0.8+y), -z)),
            0.035,
            -0.035,
            0.035,
            360.0,
            Box::new(DiffuseLight {
                texture: Arc::new(SolidColor::new(Color::saturated())),
                intensity: 4.0,
            }),
        )));
    }
    */
}

fn world_three(world: &mut Vec<Arc<dyn Shape>>) {
    //centre
    world.push(Arc::new(Disk::new(
        Arc::new(Transform::new_translate(-1.0, 0.0, 0.0)),
        Arc::new(Transform::new_translate(1.0, 0.0, 0.0)),
        0.5,
        0.25,
        0.0,
        300.0,
        Box::new(Lambertian {
            texture: Arc::new(SolidColor::new(Color::new(64, 44, 164))),
        }),
    )));

    world.push(Arc::new(Cylinder::new(
        Arc::new(Transform::new_translate(0.0, 0.125, 0.0)),
        Arc::new(Transform::new_translate(0.0, -0.125, 0.0)),
        0.50,
        0.0,
        1.0,
        360.0,
        Box::new(Lambertian {
            texture: Arc::new(SolidColor::new(Color::new(200, 64, 64))),
        }),
    )));
}
