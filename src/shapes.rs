use crate::bounds::Bounds;
use crate::color::Color;
use crate::hittable::{IntersectionEvent, IntersectionGeometry};
use crate::materials::Material;
use crate::ray::Ray;
use crate::transform::Transform;
use crate::util::gamma;
use std::sync::Arc;
use ultraviolet::Vec3;

pub mod cylinder;
pub mod disk;
pub mod sphere;

pub trait Shape: Sync + Send + Interaction {
    // return the bounding box in the Object's coordinate space
    fn object_bound(&self) -> Bounds;
    // return the bounding box in the World coordinate space
    fn world_bound(&self) -> Bounds;

    fn object_to_world(&self) -> Arc<Transform>;
    fn world_to_object(&self) -> Arc<Transform>;

    fn area(&self) -> f32;
    fn pdf(&self) -> f32;
}

pub trait Interaction: Sync + Send {
    fn intersect(&self, r: &Ray, t_max: f32) -> (IntersectionEvent, IntersectionGeometry);
    fn intersect_p(&self) -> bool;

    //fn get_material() -> Box<dyn Material>;
    //
    //if this thing is a light source
    //fn get_area_light() -> Box< dyn AreaLight>;

    fn material_scatter(
        &self,
        r_in: &Ray,
        event: &IntersectionEvent,
        i_g: &IntersectionGeometry,
    ) -> (bool, Color, Ray);
    fn emit_light(&self, i_g: &IntersectionGeometry) -> Color;
}

// TODO: this function may need to return whether there was a hit, but
// we want to use something better than a bool so that we can scale this
// to a SIMD vec3
//
// TODO: this function may be a candidate for optimization by taking in precomputed values for the
// inv direction...see pbr-book 3.1.2
pub fn intersect_p(r: &Ray, b: &Bounds) -> (f32, f32) {
    let mut t0: f32 = 0.0;
    let mut t1 = r.t_max;

    let inv_ray_dir = Vec3::new(1.0, 1.0, 1.0) / r.direction;
    let mut t_near = (b.p_min - r.origin) * inv_ray_dir;
    let mut t_fear = (b.p_max - r.origin) * inv_ray_dir;

    if t_near.x > t_fear.x {
        std::mem::swap(&mut t_near.x, &mut t_fear.x);
    }
    if t_near.y > t_fear.y {
        std::mem::swap(&mut t_near.y, &mut t_fear.y);
    }
    if t_near.z > t_fear.z {
        std::mem::swap(&mut t_near.z, &mut t_fear.z);
    }

    t_fear *= 1.0 + 2.0 * gamma(3);

    if t_near.x > t0 {
        t0 = t_near.x;
    }
    if t_fear.x < t1 {
        t1 = t_fear.x;
    }
    if t0 > t1 {
        return (-1.0, 1.0);
    }

    if t_near.y > t0 {
        t0 = t_near.y;
    }

    if t_fear.y < t1 {
        t1 = t_fear.y;
    }
    if t0 > t1 {
        return (-1.0, 1.0);
    }

    if t_near.z > t0 {
        t0 = t_near.z;
    }

    if t_fear.z < t1 {
        t1 = t_fear.z;
    }
    if t0 > t1 {
        return (-1.0, -1.0);
    }

    (t0, t1)
}
