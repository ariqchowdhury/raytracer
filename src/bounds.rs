use crate::ray::Ray;
use crate::util::gamma;
use ultraviolet::Vec3;

#[derive(Copy, Clone, Debug)]
pub struct Bounds {
    pub p_min: Vec3,
    pub p_max: Vec3,
}

// Axis-Aligned Bounding Box
impl Bounds {
    pub fn new() -> Bounds {
        Bounds {
            p_min: Vec3::new(f32::MAX, f32::MAX, f32::MAX),
            p_max: Vec3::new(f32::MIN, f32::MIN, f32::MIN),
        }
    }

    pub fn new_point(p: &Vec3) -> Bounds {
        Bounds {
            p_min: *p,
            p_max: *p,
        }
    }

    // return a new bounding box that encompasses this box
    // and the point
    pub fn union_p(&self, point: &Vec3) -> Bounds {
        Bounds {
            p_min: Vec3::new(
                f32::min(self.p_min.x, point.x),
                f32::min(self.p_min.y, point.y),
                f32::min(self.p_min.z, point.z),
            ),
            p_max: Vec3::new(
                f32::max(self.p_max.x, point.x),
                f32::max(self.p_max.y, point.y),
                f32::max(self.p_max.z, point.z),
            ),
        }
    }

    // return a new bounding box that encompasses this box
    // and another box
    pub fn union_b(&self, b: &Bounds) -> Bounds {
        Bounds {
            p_min: Vec3::new(
                f32::min(self.p_min.x, b.p_min.x),
                f32::min(self.p_min.y, b.p_min.y),
                f32::min(self.p_min.z, b.p_min.z),
            ),
            p_max: Vec3::new(
                f32::max(self.p_max.x, b.p_max.x),
                f32::max(self.p_max.y, b.p_max.y),
                f32::max(self.p_max.z, b.p_max.z),
            ),
        }
    }

    pub fn intersect(&self, b: &Bounds) -> Bounds {
        Bounds {
            p_min: Vec3::new(
                f32::max(self.p_min.x, b.p_min.x),
                f32::max(self.p_min.y, b.p_min.y),
                f32::max(self.p_min.z, b.p_min.z),
            ),
            p_max: Vec3::new(
                f32::min(self.p_max.x, b.p_max.x),
                f32::min(self.p_max.y, b.p_max.y),
                f32::min(self.p_max.z, b.p_max.z),
            ),
        }
    }

    pub fn intersect_p(&self, r: &Ray, inv_dir: &Vec3, dir_is_neg: [bool; 3]) -> bool {
        let adjust = 1.0 + 2.0 * gamma(3);
        let mut t_min = (self.p_min.x - r.origin.x) * inv_dir.x;
        let mut t_max = (self.p_max.x - r.origin.x) * inv_dir.x;

        if dir_is_neg[0] {
            std::mem::swap(&mut t_min, &mut t_max);
        }

        let mut ty_min = (self.p_min.y - r.origin.y) * inv_dir.y;
        let mut ty_max = (self.p_max.y - r.origin.y) * inv_dir.y;

        if dir_is_neg[1] {
            std::mem::swap(&mut ty_min, &mut ty_max);
        }

        t_max *= adjust;
        ty_max *= adjust;

        if t_min > ty_max || ty_min > t_max {
            return false;
        }

        if ty_min > t_min {
            t_min = ty_min;
        }
        if ty_max < t_max {
            t_max = ty_max;
        }

        let mut tz_min = (self.p_min.z - r.origin.z) * inv_dir.z;
        let mut tz_max = (self.p_max.z - r.origin.z) * inv_dir.z;

        if dir_is_neg[2] {
            std::mem::swap(&mut tz_min, &mut tz_max);
        }

        tz_max *= adjust;
        if t_min > tz_max || tz_min > t_max {
            return false;
        }

        if tz_min > t_min {
            t_min = tz_min;
        }
        if tz_max < t_max {
            t_max = tz_max;
        }

        return t_min < r.t_max && t_max > 0.0;
    }

    pub fn overlap(&self, b: &Bounds) -> bool {
        let x = self.p_max.x >= b.p_min.x && self.p_min.x <= b.p_max.x;
        let y = self.p_max.y >= b.p_min.y && self.p_min.y <= b.p_max.y;
        let z = self.p_max.z >= b.p_min.z && self.p_min.z <= b.p_max.z;

        x && y && z
    }

    // determines if the point inside this bounding box
    pub fn inside(&self, point: &Vec3) -> bool {
        point.x >= self.p_min.x
            && point.x <= self.p_max.x
            && point.y >= self.p_min.y
            && point.y <= self.p_max.y
            && point.z >= self.p_min.z
            && point.z <= self.p_max.z
    }

    // return a bounding box that is like this box, expanded by
    // a constant factor in each dimension
    pub fn expand(&self, f: f32) -> Bounds {
        Bounds {
            p_min: self.p_min - Vec3::new(f, f, f),
            p_max: self.p_max + Vec3::new(f, f, f),
        }
    }

    pub fn diagonal(&self) -> Vec3 {
        self.p_max - self.p_min
    }

    pub fn surface_area(&self) -> f32 {
        let d = self.diagonal();
        2.0 * (d.x * d.y + d.x * d.z + d.y * d.z)
    }

    pub fn volume(&self) -> f32 {
        let d = self.diagonal();
        d.x * d.y * d.z
    }

    pub fn maximum_extent(&self) -> u32 {
        let d = self.diagonal();
        if (d.x > d.y) && (d.x > d.z) {
            return 0;
        } else if d.y > d.z {
            return 1;
        } else {
            return 2;
        }
    }

    pub fn bounding_sphere(&self) -> (Vec3, f32) {
        let centre = (self.p_min + self.p_max) / 2.0;
        let radius = if self.inside(&centre) {
            (centre - self.p_max).mag()
        } else {
            0.0
        };

        (centre, radius)
    }

    // return the position of a point relative to the min corner of the box
    // ie. a normalized ray from the min corner to the point
    pub fn offset(&self, p: Vec3) -> Vec3 {
        let mut o = p - self.p_min;
        if self.p_max.x > self.p_min.x {
            o.x /= self.p_max.x - self.p_min.x;
        }
        if self.p_max.y > self.p_min.y {
            o.y /= self.p_max.y - self.p_min.y;
        }
        if self.p_max.z > self.p_min.z {
            o.z /= self.p_max.z - self.p_min.z;
        }
        o
    }
}

impl std::convert::From<&Vec3> for Bounds {
    fn from(p: &Vec3) -> Bounds {
        Bounds {
            p_min: *p,
            p_max: *p,
        }
    }
}

impl std::convert::From<(&Vec3, &Vec3)> for Bounds {
    fn from((p1, p2): (&Vec3, &Vec3)) -> Bounds {
        Bounds {
            p_min: Vec3::new(
                f32::min(p1.x, p2.x),
                f32::min(p1.y, p2.y),
                f32::min(p1.z, p2.z),
            ),
            p_max: Vec3::new(
                f32::max(p1.x, p2.x),
                f32::max(p1.y, p2.y),
                f32::max(p1.z, p2.z),
            ),
        }
    }
}
