use crate::ray::{random_in_unit_disk, Ray};
use ultraviolet::{Rotor2, Vec2, Vec3};

pub trait Camera: Sync + Send {
    fn get_ray(&self, s: f32, t: f32) -> Ray;
    //fn get_rayX8(&self, s: f32, t: f32) -> Ray;
    fn get_ray_given_pos(&self, s: f32, t: f32, u: f32, v: f32) -> Ray;
}

pub struct BasicCamera {
    pub origin: Vec3,
    pub horizontal: Vec3,
    pub vertical: Vec3,
    pub lower_left: Vec3,
    pub u: Vec3,
    pub v: Vec3,
    pub w: Vec3,
    pub lens_radius: f32,
}

impl BasicCamera {
    pub fn new(
        lookfrom: Vec3,
        lookat: Vec3,
        vup: Vec3,
        vfov: f32,
        aspect_ratio: f32,
        aperture: f32,
        focus_dist: f32,
    ) -> BasicCamera {
        let theta = vfov.to_radians();
        let h = (theta / 2.0).tan();
        let viewport_h = 2.0 * h;
        let viewport_w = aspect_ratio * viewport_h;

        // create new coordinate system
        let w = (lookfrom - lookat).normalized();
        let u = vup.cross(w).normalized();
        let v = w.cross(u);

        // calculate the viewport
        let origin = lookfrom;
        let horizontal = focus_dist * viewport_w * u;
        let vertical = focus_dist * viewport_h * v;
        let lower_left_corner = origin - horizontal / 2.0 - vertical / 2.0 - focus_dist * w;
        let lens_radius = aperture / 2.0;

        BasicCamera {
            origin: origin,
            horizontal: horizontal,
            vertical: vertical,
            lower_left: lower_left_corner,
            u: u,
            v: v,
            w: w,
            lens_radius: lens_radius,
        }
    }
}

impl Camera for BasicCamera {
    fn get_ray(&self, s: f32, t: f32) -> Ray {
        let rd = self.lens_radius * random_in_unit_disk();
        let offset = self.u * rd.x + self.v * rd.y;
        Ray::new(
            self.origin + offset,
            self.lower_left + s * self.horizontal + t * self.vertical - self.origin - offset,
        )
    }

    fn get_ray_given_pos(&self, s: f32, t: f32, r: f32, theta: f32) -> Ray {
        let rad = Vec2::new(self.lens_radius * r, 0.0);
        let rot = Rotor2::from_angle(theta);
        let unit_offset = rot * rad;

        let offset = self.u * unit_offset.x + self.v * unit_offset.y;
        Ray::new(
            self.origin + offset,
            self.lower_left + s * self.horizontal + t * self.vertical - self.origin - offset,
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::Rng;

    #[test]
    fn test_lens_pos_gen() {
        let mut rng = rand::thread_rng();

        for _ in 0..100 {
            let r = rng.gen_range(0.0..1.0);
            let theta = 360.0 * rng.gen_range(0.0..1.0);

            let s = Rotor2::from_angle(theta);
            let v = Vec2::new(r, 0.0);
            let q = s * v;

            assert!(q.mag_sq() <= 1.0);
            assert!(q.x <= 1.0 && q.x >= -1.0);
            assert!(q.y <= 1.0 && q.y >= -1.0);
        }
    }
}
