#[inline]
pub fn clamp<T: PartialOrd>(val: T, low: T, high: T) -> T {
    if val < low {
        low
    } else if val > high {
        high
    } else {
        val
    }
}

#[inline]
pub fn gamma(n: i32) -> f32 {
    (n as f32 * f32::EPSILON) / (1.0 - n as f32 * f32::EPSILON)
}
