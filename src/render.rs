use crate::camera::Camera;
use crate::color::Color;
use crate::integrator::{Lights, Tracer, World};
use crate::sampler::radical_inverse;
use bytemuck::cast as unsafe_byte_cast;
use rayon::prelude::*;
use std::iter::Sum;
use std::sync::atomic::{AtomicU32, Ordering};
use ultraviolet::{f32x4, Vec2};
use wide::{CmpGt, CmpLt};

pub const ASPECT_RATIO: f32 = 16.0 / 9.0;
pub const WIDTH: u32 = 1024;
pub const HEIGHT: u32 = (WIDTH as f32 / ASPECT_RATIO) as u32;
pub const NUM_PIXELS: usize = WIDTH as usize * HEIGHT as usize * 4 as usize;

pub struct Frame {
    pub f: Box<[f32; NUM_PIXELS]>,
}

pub struct FrameRow {
    pub r: Box<[f32; (WIDTH * 4) as usize]>,
}

pub fn halton_render_frame<F: Filter>(
    c: &impl Camera,
    integrator: impl Tracer,
    world: &impl World,
    lights: &impl Lights,
    samples_per_pixel: u32,
    filter: &F,
) -> Frame {
    let frame_id = AtomicU32::new(0);

    let mut all_rows: Vec<(u32, FrameRow)> = (0..HEIGHT)
        .into_par_iter()
        .map(|_| {
            halton_render_frame_(
                c,
                integrator,
                world,
                lights,
                &frame_id,
                samples_per_pixel,
                filter,
            )
        })
        .collect();

    use std::time::Instant;
    let now = Instant::now();

    all_rows.sort_by(|(a, _), (b, _)| a.partial_cmp(b).unwrap());

    let mut temp = Vec::with_capacity(NUM_PIXELS);

    for r in all_rows {
        temp.extend(&*r.1.r);
    }

    let mut f = Frame {
        f: Box::new([0.0; NUM_PIXELS]),
    };

    f.f.copy_from_slice(&temp);

    let elapsed = now.elapsed();
    println!("frame summation time: {:.2?}", elapsed);

    f
}

pub fn halton_render_frame_<F: Filter>(
    c: &impl Camera,
    integrator: impl Tracer,
    world: &impl World,
    lights: &impl Lights,
    frame_id: &AtomicU32,
    samples_per_pixel: u32,
    filter: &F,
) -> (u32, FrameRow) {
    let mut row = FrameRow {
        r: Box::new([0.0; (WIDTH * 4) as usize]),
    };

    let sample_num = frame_id.fetch_add(1, Ordering::Relaxed);
    let total_samples = samples_per_pixel * WIDTH;

    let mut halton_pixel = Vec::<(f32, f32)>::with_capacity(total_samples as usize);
    let mut halton_camera = Vec::<(f32, f32)>::with_capacity(total_samples as usize);

    // get pixel (x, y) to sample from halton sequence
    // get camera offset from halton sequence
    // need NUM_PIXELS/4 number of value. sample_num * (NUM_PIXELS/4)
    let range = (sample_num * WIDTH as u32) as usize;
    for n in range..range + total_samples as usize {
        halton_pixel.push((radical_inverse(2, n as u64), radical_inverse(3, n as u64)));
        halton_camera.push((radical_inverse(7, n as u64), radical_inverse(5, n as u64)));
    }

    for (i, pixel) in row.r.chunks_exact_mut(4).enumerate() {
        let (mut lum, f_sum) = (0..samples_per_pixel)
            .map(|s| {
                let k = i;
                let j = sample_num;

                let idx = i * samples_per_pixel as usize + s as usize;
                let (mut x, mut y) = halton_pixel[idx];

                x *= 2.0;
                y *= 2.0;

                let u: f32 = (k as f32 + x) / (WIDTH as f32 - 1.0);
                let v: f32 = (j as f32 + y) / (HEIGHT as f32 - 1.0);

                let (cr, ct) = halton_camera[idx];

                //let color = integrator.trace(&c.get_ray_given_pos(u, v, cr, ct), world, lights);
                let color = integrator.trace(&c.get_ray(u, v), world, lights);

                (1.0 - x, 1.0 - y, color)
            })
            .fold((Color::black(), 0.0), |(l, fsum), (w, h, c)| {
                let filt = filter.evaluate(Vec2::new(w, h));
                (l + (filt * c), fsum + filt)
            });

        lum = (1.0 / f_sum) * lum;

        pixel[0] = lum.0.x;
        pixel[1] = lum.0.y;
        pixel[2] = lum.0.z;
        pixel[3] = 1.0;
    }

    println!("row {}/{}", sample_num, HEIGHT);
    (sample_num, row)
}

#[inline]
pub fn compute_color_wide(c: f32x4, gamma: f32) -> [f32; 4] {
    let scale = f32x4::splat(gamma);
    let mut result = c * scale;
    result = result.sqrt();

    // clamp values to range of 0.0 to 0.999
    let min_mask = result.cmp_lt(f32x4::splat(0.0));
    result = min_mask.blend(f32x4::splat(0.0), result);

    let max_mask = result.cmp_gt(f32x4::splat(0.999));
    result = max_mask.blend(f32x4::splat(0.999), result);

    result = result * 256.0;

    return unsafe_byte_cast(result);
}

pub fn frame_add(mut a: Frame, b: Frame) -> Frame {
    for (aval, bval) in a.f.iter_mut().zip(b.f.iter()) {
        *aval += bval
    }
    a
}

impl Sum for Frame {
    fn sum<I>(iter: I) -> Self
    where
        I: Iterator<Item = Self>,
    {
        iter.fold(
            Frame {
                f: Box::new([0.0; NUM_PIXELS]),
            },
            frame_add,
        )
    }
}

pub trait Filter: Sync + Send + Copy {
    fn evaluate(self, p: Vec2) -> f32;
}

#[derive(Copy, Clone)]
pub struct BoxFilter {}

impl Filter for BoxFilter {
    fn evaluate(self, p: Vec2) -> f32 {
        1.0
    }
}

#[derive(Copy, Clone)]
pub struct TriangleFilter {
    pub radius: f32,
}

impl TriangleFilter {
    pub fn new(r: f32) -> TriangleFilter {
        TriangleFilter { radius: r }
    }
}

impl Filter for TriangleFilter {
    fn evaluate(self, p: Vec2) -> f32 {
        0.0_f32.max(self.radius - p.x.abs()) * 0.0_f32.max(self.radius - p.y.abs())
    }
}

#[derive(Copy, Clone)]
pub struct GaussianFilter {
    pub alpha: f32,
    pub radius: f32,
    pub exp: f32,
}

impl GaussianFilter {
    pub fn new(r: f32, a: f32) -> GaussianFilter {
        GaussianFilter {
            radius: r,
            alpha: a,
            exp: (-a * r * r).exp(),
        }
    }

    fn gaussian(self, d: f32) -> f32 {
        0.0_f32.max((-self.alpha * d * d).exp() - self.exp)
    }
}

impl Filter for GaussianFilter {
    fn evaluate(self, p: Vec2) -> f32 {
        self.gaussian(p.x) * self.gaussian(p.y)
    }
}

#[derive(Copy, Clone)]
pub struct MitchellFilter {
    pub radius: f32,
    pub inv_radius: f32,
    pub b: f32,
    pub c: f32,
}

impl MitchellFilter {
    pub fn new(r: f32, b: f32, c: f32) -> MitchellFilter {
        MitchellFilter {
            radius: r,
            inv_radius: 1.0 / r,
            b: b,
            c: c,
        }
    }

    fn mitchell_1d(self, d: f32) -> f32 {
        let x = (2.0 * d).abs();

        if x > 1.0 {
            return ((-self.b - 6.0 * self.c) * x * x * x
                + (6.0 * self.b + 30.0 * self.c) * x * x
                + (-12.0 * self.b - 48.0 * self.c) * x
                + (8.0 * self.b + 24.0 * self.c))
                * (1.0 / 6.0);
        } else {
            return ((12.0 - 9.0 * self.b - 6.0 * self.c) * x * x * x
                + (-18.0 + 12.0 * self.b + 6.0 * self.c) * x * x
                + (6.0 - 2.0 * self.b))
                * (1.0 / 6.0);
        }
    }
}

impl Filter for MitchellFilter {
    fn evaluate(self, p: Vec2) -> f32 {
        self.mitchell_1d(p.x * self.inv_radius) * self.mitchell_1d(p.y * self.inv_radius)
    }
}
