use crate::color::Color;
use crate::ray::Ray;

pub trait LightEmission: Sync + Send {
    fn emit_light(&self, r: &Ray) -> Color;
}

pub struct GlobalInfiniteLight {
    pub lerp_color_start: Color,
    pub lerp_color_end: Color,
}

impl LightEmission for GlobalInfiniteLight {
    fn emit_light(&self, r: &Ray) -> Color {
        let direction = r.direction.normalized();
        let t = 0.5 * (direction.y + 1.0);
        (1.0 - t) * self.lerp_color_start + t * self.lerp_color_end
    }
}
