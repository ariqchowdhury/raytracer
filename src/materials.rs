use crate::color::Color;
use crate::hittable::{IntersectionEvent, IntersectionGeometry};
use crate::ray::{random_in_unit_sphere, random_unit_vector, Ray};
use crate::texture::Texture;
use std::sync::Arc;
use ultraviolet::Vec3;

pub trait Material: Sync + Send {
    fn scatter(
        &self,
        r_in: &Ray,
        event: &IntersectionEvent,
        i_g: &IntersectionGeometry,
    ) -> (bool, Color, Ray);
    fn emit_light(&self, i_g: &IntersectionGeometry) -> Color {
        Color::black()
    }
}

#[derive(Clone)]
pub struct Lambertian {
    pub texture: Arc<dyn Texture>,
}

impl Material for Lambertian {
    fn scatter(
        &self,
        _r_in: &Ray,
        event: &IntersectionEvent,
        i_g: &IntersectionGeometry,
    ) -> (bool, Color, Ray) {
        let mut scatter_direction = event.normal + random_unit_vector();
        if near_zero(scatter_direction) {
            scatter_direction = event.normal;
        }
        let scattered = Ray::new(event.p, scatter_direction);
        let attenuation = self.texture.value(i_g.uv.x, i_g.uv.y, i_g.p);
        return (true, attenuation, scattered);
    }
}

fn near_zero(v: Vec3) -> bool {
    let s: f32 = 1e-8;
    v.x.abs() < s && v.y.abs() < s && v.z.abs() < s
}

#[derive(Clone)]
pub struct Metal {
    pub albedo: Color,
    pub fuzz: f32,
}

impl Material for Metal {
    fn scatter(
        &self,
        r_in: &Ray,
        event: &IntersectionEvent,
        i_g: &IntersectionGeometry,
    ) -> (bool, Color, Ray) {
        let reflected = r_in.direction.normalized().reflected(event.normal);
        let scattered = Ray::new(event.p, reflected + self.fuzz * random_in_unit_sphere());
        return (
            scattered.direction.dot(event.normal) > 0.0,
            self.albedo,
            scattered,
        );
    }
}

#[derive(Clone)]
pub struct DiffuseLight {
    pub texture: Arc<dyn Texture>,
    pub intensity: f32,
}

impl Material for DiffuseLight {
    fn scatter(
        &self,
        _r_in: &Ray,
        event: &IntersectionEvent,
        i_g: &IntersectionGeometry,
    ) -> (bool, Color, Ray) {
        let mut scatter_direction = event.normal + random_unit_vector();
        if near_zero(scatter_direction) {
            scatter_direction = event.normal;
        }
        let scattered = Ray::new(event.p, scatter_direction);
        //let attenuation = self.texture.value(i_g.uv.x, i_g.uv.y, i_g.p);
        let attenuation = Color::black();
        return (true, attenuation, scattered);
    }

    fn emit_light(&self, i_g: &IntersectionGeometry) -> Color {
        self.intensity * self.texture.value(i_g.uv.x, i_g.uv.y, i_g.p)
    }
}
