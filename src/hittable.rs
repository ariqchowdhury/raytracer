use crate::color::Color;
use crate::ray::Ray;
use ultraviolet::{Vec2, Vec3};

#[derive(Copy, Clone)]
pub struct IntersectionEvent {
    pub t: f32,
    pub p: Vec3,
    pub normal: Vec3,
    pub front_face: bool,
}

impl IntersectionEvent {
    pub fn new() -> IntersectionEvent {
        IntersectionEvent {
            t: 0.0,
            p: Vec3::zero(),
            normal: Vec3::zero(),
            front_face: false,
        }
    }
}

pub trait Hittable: Sync + Send {
    fn hit(&self, r: &Ray, t_min: f32, t_max: f32, event: &mut IntersectionEvent) -> bool;
    fn material_scatter(&self, r_in: &Ray, event: &IntersectionEvent) -> (bool, Color, Ray);
}

#[derive(Copy, Clone)]
pub struct IntersectionGeometry {
    // point of interaction
    pub p: Vec3,
    pub p_error: Vec3,
    // for interaction along a ray, this stores the negative ray direction
    pub wo: Vec3,
    // the surface normal at the point p
    pub n: Vec3,

    pub uv: Vec2,
    pub dpdu: Vec3,
    pub dpdv: Vec3,
    pub dndu: Vec3,
    pub dndv: Vec3,
}

impl IntersectionGeometry {
    pub fn new(
        p: Vec3,
        p_e: Vec3,
        wo: Vec3,
        uv: Vec2,
        dpdu: Vec3,
        dpdv: Vec3,
        dndu: Vec3,
        dndv: Vec3,
    ) -> IntersectionGeometry {
        let norm = dpdu.cross(dpdv).normalized();

        IntersectionGeometry {
            p: p,
            p_error: p_e,
            wo: wo,
            n: norm,
            uv: uv,
            dpdu: dpdu,
            dpdv: dpdv,
            dndu: dndu,
            dndv: dndv,
        }
    }

    pub fn empty() -> IntersectionGeometry {
        IntersectionGeometry {
            p: Vec3::zero(),
            p_error: Vec3::zero(),
            wo: Vec3::zero(),
            n: Vec3::zero(),
            uv: Vec2::zero(),
            dpdu: Vec3::zero(),
            dpdv: Vec3::zero(),
            dndu: Vec3::zero(),
            dndv: Vec3::zero(),
        }
    }
}
