use crate::bounds::Bounds;
use crate::color::Color;
use crate::hittable::{IntersectionEvent, IntersectionGeometry};
use crate::integrator::World;
use crate::ray::Ray;
use crate::shapes::Shape;
use std::rc::Rc;
use std::sync::Arc;
use ultraviolet::Vec3;

pub struct BVHPrimitiveInfo {
    prim_index: usize,
    bounds: Bounds,
    centroid: Vec3,
}

impl BVHPrimitiveInfo {
    pub fn new(n: usize, b: Bounds) -> BVHPrimitiveInfo {
        BVHPrimitiveInfo {
            prim_index: n,
            bounds: b,
            centroid: 0.5 * b.p_min + 0.5 * b.p_max,
        }
    }
}

#[derive(Clone)]
pub struct BVHBuildNode {
    bounds: Bounds,

    l: Option<Rc<BVHBuildNode>>,
    r: Option<Rc<BVHBuildNode>>,

    split_axis: u32,
    num_primitives: usize,
    first_prim_offset: usize,
}

#[derive(Debug)]
pub struct BVHLinearNode {
    bounds: Bounds,
    offset: u32,
    num_prim: u16,
    axis: u8,
    _pad: u8,
}

impl BVHLinearNode {
    fn new() -> BVHLinearNode {
        BVHLinearNode {
            bounds: Bounds::new(),
            offset: 0,
            num_prim: 0,
            axis: 0,
            _pad: 0,
        }
    }
}

impl BVHBuildNode {
    pub fn init_leaf(first: usize, n: usize, b: &Bounds) -> BVHBuildNode {
        BVHBuildNode {
            bounds: *b,
            l: None,
            r: None,

            split_axis: 0,
            num_primitives: n,
            first_prim_offset: first,
        }
    }

    pub fn init_interior(&mut self, axis: u32, l: Rc<BVHBuildNode>, r: Rc<BVHBuildNode>) -> () {
        self.l = Some(l);
        self.r = Some(r);

        self.bounds = self
            .l
            .as_ref()
            .unwrap()
            .bounds
            .union_b(&self.r.as_ref().unwrap().bounds);
        self.split_axis = axis;
        self.num_primitives = 0;
    }
}

struct LBVHTreelet {
    start_index: usize,
    num_prims: usize,
    build_nodes: Option<Rc<BVHBuildNode>>,
}

#[inline]
fn left_shift(x: u32) -> u32 {
    let mut y = if x == (1 << 10) { x - 1 } else { x };

    y = (y | (y << 16)) & 0b00000011000000000000000011111111;
    y = (y | (y << 8)) & 0b00000011000000001111000000001111;
    y = (y | (y << 4)) & 0b00000011000011000011000011000011;
    y = (y | (y << 2)) & 0b00001001001001001001001001001001;
    return y;
}

#[inline]
fn encode_morton(v: &Vec3) -> u32 {
    return (left_shift(v.z as u32) << 2)
        | (left_shift(v.y as u32) << 1)
        | (left_shift(v.x as u32));
}

pub struct BVHAccelWorld {
    pub primitives: Vec<Arc<dyn Shape>>,
    pub max_prim_in_node: u32,
    pub nodes: Vec<BVHLinearNode>,
}

impl BVHAccelWorld {
    pub fn new(p: Vec<Arc<dyn Shape>>, max_prim: u32) -> BVHAccelWorld {
        BVHAccelWorld {
            primitives: p,
            max_prim_in_node: std::cmp::max(255, max_prim),
            nodes: Vec::new(),
        }
    }

    pub fn init(&mut self) {
        let mut p_i: Vec<BVHPrimitiveInfo> = Vec::with_capacity(self.primitives.len());

        for (i, p) in self.primitives.iter().enumerate() {
            p_i.push(BVHPrimitiveInfo::new(i, p.world_bound()));
        }

        let mut ordered_prims: Vec<Arc<dyn Shape>> = Vec::new();
        let (root, total_nodes) = self.hlbvh_build(&p_i, &mut ordered_prims);

        assert!(self.primitives.len() == ordered_prims.len());
        self.primitives.swap_with_slice(&mut ordered_prims);

        let mut offset = 0;
        self.flatten_tree(&root, &mut offset);
        assert_eq!(total_nodes as u32, offset);
    }

    // Hierarchical Linear Bounding Volume Hierarchy, pbrt book 3ed 4.3.3
    fn hlbvh_build(
        &mut self,
        p_i: &Vec<BVHPrimitiveInfo>,
        ordered_prims: &mut Vec<Arc<dyn Shape>>,
    ) -> (Rc<BVHBuildNode>, usize) {
        let mut b = Bounds::new();
        for p in p_i {
            b = b.union_p(&p.centroid);
        }

        let mut mp: Vec<MortonPrimitive> = vec![
            MortonPrimitive {
                prim_index: 0,
                code: 0
            };
            p_i.len()
        ];
        for (i, p) in p_i.iter().enumerate() {
            let morton_bits = 10;
            let morton_scale: u32 = 1 << morton_bits;
            mp[i].prim_index = p.prim_index;
            let centroid_offset = b.offset(p.centroid);
            mp[i].code = encode_morton(&(morton_scale as f32 * centroid_offset));
        }

        // TODO: different sorting algorithm may be faster here?
        mp.sort_by(|a, b| b.code.cmp(&a.code));

        let mut treelets_to_build: Vec<LBVHTreelet> = Vec::new();

        let mut start = 0;
        let mut end = 1;
        for _ in mp.iter() {
            let mask: u32 = 0b00111111111111000000000000000000;
            if end >= mp.len() || (mp[start].code & mask) != (mp[end].code & mask) {
                let num_primitives = end - start;
                treelets_to_build.push(LBVHTreelet {
                    start_index: start,
                    num_prims: num_primitives,
                    build_nodes: None,
                });
                start = end;
            }

            end += 1;
        }

        let mut node_total = 0;
        let mut ordered_prim_offset: usize = 0;
        for t in treelets_to_build.iter_mut() {
            let mut nodes_created = 0;
            let first_bit_index: i32 = 29 - 12;
            t.build_nodes = Some(self.emit_lbvh(
                p_i,
                &mp[t.start_index..],
                t.num_prims,
                &mut nodes_created,
                ordered_prims,
                &mut ordered_prim_offset,
                first_bit_index,
            ));
            node_total += nodes_created;
        }

        let mut finished_treelets: Vec<Rc<BVHBuildNode>> =
            Vec::with_capacity(treelets_to_build.len());
        for t in treelets_to_build.into_iter() {
            finished_treelets.push(t.build_nodes.unwrap());
        }

        // Create a SAH BVH from the treelets
        let treelet_size = finished_treelets.len();
        let bvh = self.build_upper_sah(&mut finished_treelets, 0, treelet_size, &mut node_total);
        (bvh, node_total as usize)
    }

    fn emit_lbvh(
        &mut self,
        prim_info: &Vec<BVHPrimitiveInfo>,
        morton_prims: &[MortonPrimitive],
        num_prims: usize,
        total_nodes: &mut u32,
        ordered_prims: &mut Vec<Arc<dyn Shape>>,
        ordered_prim_offset: &mut usize,
        bit_index: i32,
    ) -> Rc<BVHBuildNode> {
        if (bit_index == -1) || (num_prims < self.max_prim_in_node as usize) {
            *total_nodes += 1;
            let first_prim_offset = *ordered_prim_offset;
            *ordered_prim_offset += num_prims;
            let mut bounds = Bounds::new();
            for i in 0..num_prims {
                let prim_index = morton_prims[i].prim_index;
                ordered_prims.push(self.primitives[prim_index].clone());
                bounds = bounds.union_b(&prim_info[prim_index].bounds);
            }
            return Rc::new(BVHBuildNode::init_leaf(
                first_prim_offset,
                num_prims,
                &bounds,
            ));
        } else {
            let mask = 1 << bit_index;
            if (morton_prims[0].code & mask) == (morton_prims[num_prims - 1].code & mask) {
                return self.emit_lbvh(
                    prim_info,
                    morton_prims,
                    num_prims,
                    total_nodes,
                    ordered_prims,
                    ordered_prim_offset,
                    bit_index - 1,
                );
            }

            let mut search_start = 0;
            let mut search_end = num_prims - 1;
            while (search_start + 1) != search_end {
                let mid = (search_start + search_end) / 2;
                if (morton_prims[search_start].code & mask) == (morton_prims[mid].code & mask) {
                    search_start = mid;
                } else {
                    search_end = mid;
                }
            }
            let split_offset = search_end;

            *total_nodes += 1;
            let mut bn = BVHBuildNode::init_leaf(0, 0, &Bounds::new());
            let l = self.emit_lbvh(
                prim_info,
                morton_prims,
                split_offset,
                total_nodes,
                ordered_prims,
                ordered_prim_offset,
                bit_index - 1,
            );
            let r = self.emit_lbvh(
                prim_info,
                &morton_prims[split_offset..],
                num_prims - split_offset,
                total_nodes,
                ordered_prims,
                ordered_prim_offset,
                bit_index - 1,
            );
            let axis = bit_index % 3;
            bn.init_interior(axis as u32, l, r);
            return Rc::new(bn);
        }
    }

    fn build_upper_sah(
        &self,
        treelet_roots: &mut Vec<Rc<BVHBuildNode>>,
        start: usize,
        end: usize,
        total_nodes: &mut u32,
    ) -> Rc<BVHBuildNode> {
        assert!(start < end);
        let n_nodes = end - start;
        if n_nodes == 1 {
            return treelet_roots[start].clone();
        }
        *total_nodes += 1;

        let mut bounds = Bounds::new();
        for i in start..end {
            bounds = bounds.union_b(&treelet_roots[i].bounds);
        }

        let mut centroid_bounds = Bounds::new();
        for i in start..end {
            let centroid = 0.5 * (treelet_roots[i].bounds.p_min + treelet_roots[i].bounds.p_max);
            centroid_bounds = centroid_bounds.union_p(&centroid);
        }
        let dim = centroid_bounds.maximum_extent();

        #[derive(Copy, Clone)]
        struct BucketInfo {
            count: u32,
            bounds: Bounds,
        }
        // number of partitions for sah
        const NUM_BUCKETS: usize = 12;
        let mut buckets = [BucketInfo {
            count: 0,
            bounds: Bounds::new(),
        }; NUM_BUCKETS];
        for i in start..end {
            let centroid = if dim == 0 {
                0.5 * (treelet_roots[i].bounds.p_min.x + treelet_roots[i].bounds.p_max.x)
            } else if dim == 1 {
                0.5 * (treelet_roots[i].bounds.p_min.y + treelet_roots[i].bounds.p_max.y)
            } else {
                0.5 * (treelet_roots[i].bounds.p_min.z + treelet_roots[i].bounds.p_max.z)
            };

            let (p_min, p_max) = if dim == 0 {
                (centroid_bounds.p_min.x, centroid_bounds.p_max.x)
            } else if dim == 1 {
                (centroid_bounds.p_min.y, centroid_bounds.p_max.y)
            } else {
                (centroid_bounds.p_min.z, centroid_bounds.p_max.z)
            };

            let mut b: usize = NUM_BUCKETS * ((centroid - p_min) / (p_max - p_min)) as usize;

            if b == NUM_BUCKETS {
                b = NUM_BUCKETS - 1;
            }
            assert!(b < NUM_BUCKETS);

            buckets[b].count += 1;
            buckets[b].bounds = buckets[b].bounds.union_b(&treelet_roots[i].bounds);
        }

        let mut cost = [0.0; NUM_BUCKETS - 1];
        for i in 0..NUM_BUCKETS - 1 {
            let mut b0 = Bounds::new();
            let mut count0 = 0;
            for j in 0..i + 1 {
                b0 = b0.union_b(&buckets[j].bounds);
                count0 += buckets[j].count;
            }

            let mut b1 = Bounds::new();
            let mut count1 = 0;
            for j in i + 1..NUM_BUCKETS {
                b1 = b1.union_b(&buckets[j].bounds);
                count1 += buckets[j].count;
            }

            cost[i] = 0.125
                + (count0 as f32 * b0.surface_area() + count1 as f32 * b1.surface_area())
                    / bounds.surface_area();
        }

        let mut min_cost = cost[0];
        let mut min_cost_split_bucket = 0;
        for i in 1..NUM_BUCKETS - 1 {
            if cost[i] < min_cost {
                min_cost = cost[i];
                min_cost_split_bucket = i;
            }
        }

        let (lt, gt): (Vec<Rc<BVHBuildNode>>, Vec<Rc<BVHBuildNode>>) =
            treelet_roots[start..end].iter().cloned().partition(|node| {
                let centroid = if dim == 0 {
                    0.5 * (node.bounds.p_min.x + node.bounds.p_max.x)
                } else if dim == 1 {
                    0.5 * (node.bounds.p_min.y + node.bounds.p_max.y)
                } else {
                    0.5 * (node.bounds.p_min.z + node.bounds.p_max.z)
                };

                let (p_min, p_max) = if dim == 0 {
                    (centroid_bounds.p_min.x, centroid_bounds.p_max.x)
                } else if dim == 1 {
                    (centroid_bounds.p_min.y, centroid_bounds.p_max.y)
                } else {
                    (centroid_bounds.p_min.z, centroid_bounds.p_max.z)
                };

                let mut b: usize = NUM_BUCKETS * ((centroid - p_min) / (p_max - p_min)) as usize;

                if b == NUM_BUCKETS {
                    b = NUM_BUCKETS - 1;
                }

                b <= min_cost_split_bucket
            });

        let mid = start + lt.len();
        for i in start..mid {
            treelet_roots[i] = lt[i - start].clone();
        }

        for i in mid..mid + gt.len() {
            treelet_roots[i] = gt[i - mid].clone();
        }

        assert!(mid > start);
        assert!(mid < end);

        let mut node = BVHBuildNode {
            split_axis: 0,
            num_primitives: 0,
            first_prim_offset: 0,
            bounds: Bounds::new(),
            l: None,
            r: None,
        };
        node.init_interior(
            dim,
            self.build_upper_sah(treelet_roots, start, mid, total_nodes),
            self.build_upper_sah(treelet_roots, mid, end, total_nodes),
        );

        return Rc::new(node);
    }

    fn flatten_tree(&mut self, node: &Rc<BVHBuildNode>, offset: &mut u32) -> u32 {
        let mut linear_node = BVHLinearNode::new();
        linear_node.bounds = node.bounds;
        let my_offset = *offset;
        *offset += 1;
        if node.num_primitives > 0 {
            linear_node.offset = node.first_prim_offset as u32;
            linear_node.num_prim = node.num_primitives as u16;
            self.nodes.push(linear_node);
        } else {
            linear_node.axis = node.split_axis as u8;
            linear_node.num_prim = 0;
            self.nodes.push(linear_node);
            let this_node_idx = self.nodes.len() - 1;
            if node.l.is_some() {
                self.flatten_tree(node.l.as_ref().unwrap(), offset);
            }
            if node.r.is_some() {
                self.nodes[this_node_idx].offset =
                    self.flatten_tree(node.r.as_ref().unwrap(), offset);
            }
        }
        my_offset
    }

    pub fn intersect(&self, r: &Ray) -> (bool, IntersectionEvent, usize, IntersectionGeometry) {
        let mut hit = false;
        let mut event = IntersectionEvent::new();
        let mut inter = IntersectionGeometry::empty();
        let inv_dir = Vec3::new(1.0, 1.0, 1.0) / r.direction;
        let dir_is_neg: [bool; 3] = [inv_dir.x < 0.0, inv_dir.y < 0.0, inv_dir.z < 0.0];

        let mut to_visit_offset = 0;
        let mut curr_node_idx = 0;
        let mut hit_idx = 0;
        let mut nodes_to_visit = [0; 128];
        let mut closest_so_far = f32::INFINITY;
        loop {
            let node = &self.nodes[curr_node_idx];
            if node.bounds.intersect_p(r, &inv_dir, dir_is_neg) {
                if node.num_prim > 0 {
                    for i in 0..node.num_prim {
                        let k = (node.offset + i as u32) as usize;
                        let (e, int) = self.primitives[k].intersect(r, closest_so_far);
                        if e.t > 0.0 {
                            hit = true;
                            closest_so_far = e.t;
                            hit_idx = k;
                            event = e;
                            inter = int;
                        }
                    }
                    if to_visit_offset == 0 {
                        break;
                    }
                    to_visit_offset -= 1;
                    curr_node_idx = nodes_to_visit[to_visit_offset];
                } else {
                    if dir_is_neg[node.axis as usize] {
                        nodes_to_visit[to_visit_offset] = curr_node_idx + 1;
                        to_visit_offset += 1;
                        curr_node_idx = node.offset as usize;
                    } else {
                        nodes_to_visit[to_visit_offset] = node.offset as usize;
                        to_visit_offset += 1;
                        curr_node_idx += 1;
                    }
                }
            } else {
                if to_visit_offset == 0 {
                    break;
                }
                to_visit_offset -= 1;
                curr_node_idx = nodes_to_visit[to_visit_offset];
            }
        }

        (hit, event, hit_idx, inter)
    }
}

impl World for BVHAccelWorld {
    fn find_intersection(&self, r: &Ray) -> (bool, bool, Color, Color, Ray) {
        let (hit, event, idx, inter) = self.intersect(r);
        let mut is_scatter = false;
        let mut attenuation = Color::new(0, 0, 0);
        let mut scattered = Ray::new(Vec3::zero(), Vec3::zero());
        if hit {
            let (a, b, c) = self.primitives[idx].material_scatter(r, &event, &inter);
            is_scatter = a;
            attenuation = b;
            scattered = c;
        }
        let emitted = self.primitives[idx].emit_light(&inter);

        return (hit, is_scatter, attenuation, emitted, scattered);
    }
}

#[derive(Copy, Clone)]
struct MortonPrimitive {
    prim_index: usize,
    code: u32,
}
