use crate::color::Color;
use crate::light::LightEmission;
use crate::ray::Ray;
use crate::shapes::Shape;
use std::sync::Arc;
use ultraviolet::Vec3;

/// A Tracer defines what is necessary to trace the path of a ray from a point
/// through a scene and determine what objects in the world are intersected.
///
/// A Tracer requires a World and a Lights
pub trait Tracer: Sync + Send + Copy + Clone {
    fn trace(&self, r: &Ray, world: &impl World, lights: &impl Lights) -> Color;
}

pub trait World: Sync + Send {
    fn find_intersection(&self, r: &Ray) -> (bool, bool, Color, Color, Ray);
}

pub trait Lights: Sync + Send {
    fn find_intersection(&self, r: &Ray) -> Color;
}

#[derive(Copy, Clone)]
pub struct Whitted {
    pub depth: i32,
}

impl Whitted {
    fn trace_recursive(
        &self,
        r: &Ray,
        world: &impl World,
        lights: &impl Lights,
        depth: i32,
    ) -> Color {
        if depth <= 0 {
            return Color::black();
        }
        let (it_hit, it_scatter, attenuation, emitted, scattered) = world.find_intersection(r);

        if it_hit {
            if it_scatter {
                return emitted
                    + attenuation * self.trace_recursive(&scattered, world, lights, depth - 1);
            } else {
                return emitted;
            }
        }

        lights.find_intersection(r)
    }
}

impl Tracer for Whitted {
    fn trace(&self, r: &Ray, world: &impl World, lights: &impl Lights) -> Color {
        return self.trace_recursive(r, world, lights, self.depth);
    }
}

pub struct BruteForceWorld {
    pub scene: Vec<Arc<dyn Shape>>,
}

impl World for BruteForceWorld {
    fn find_intersection(&self, r: &Ray) -> (bool, bool, Color, Color, Ray) {
        let mut hit_anything = false;
        let mut closest_so_far = f32::INFINITY;
        let mut is_scatter = false;
        let mut attenuation = Color::new(0, 0, 0);
        let mut emitted = Color::new(0, 0, 0);
        let mut scattered = Ray::new(Vec3::zero(), Vec3::zero());

        for object in &self.scene {
            let (event, inter) = object.intersect(r, closest_so_far);
            if event.t > 0.0 {
                hit_anything = true;
                closest_so_far = event.t;
                let (a, b, c) = object.material_scatter(r, &event, &inter);
                is_scatter = a;
                attenuation = b;
                scattered = c;
                emitted = object.emit_light(&inter);
            }
        }

        return (hit_anything, is_scatter, attenuation, emitted, scattered);
    }
}

pub struct BruteForceLights {
    pub lights: Vec<Box<dyn LightEmission>>,
}

impl Lights for BruteForceLights {
    fn find_intersection(&self, r: &Ray) -> Color {
        self.lights.iter().fold(Color::black(), |mut a, b| {
            a += b.emit_light(r);
            a
        })
    }
}
