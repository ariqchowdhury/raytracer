use rand::Rng;
use std::fmt;
use std::iter::Sum;
use std::ops;
use ultraviolet::Vec3;

#[derive(Copy, Clone, Debug)]
pub struct Color(pub Vec3);

impl Color {
    pub fn r(&self) -> u8 {
        (self.0.x * 256.0) as u8
    }

    pub fn g(&self) -> u8 {
        (self.0.y * 256.0) as u8
    }

    pub fn b(&self) -> u8 {
        (self.0.z * 256.0) as u8
    }

    pub fn new(x: u8, y: u8, z: u8) -> Color {
        Color(Vec3::new(
            x as f32 / 256.0,
            y as f32 / 256.0,
            z as f32 / 256.0,
        ))
    }

    pub fn random() -> Color {
        let mut rng = rand::thread_rng();
        Color(Vec3::new(
            rng.gen_range(0.0..1.0),
            rng.gen_range(0.0..1.0),
            rng.gen_range(0.0..1.0),
        ))
    }

    pub fn black() -> Color {
        Color(Vec3::new(0.0, 0.0, 0.0))
    }

    pub fn saturated() -> Color {
        Color(Vec3::new(1.0, 1.0, 1.0))
    }
}

impl fmt::Display for Color {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "c({}, {}, {})", self.r(), self.g(), self.b())
    }
}

impl ops::Mul<Color> for f32 {
    type Output = Color;

    fn mul(self, rhs: Color) -> Color {
        Color(Vec3 {
            x: self * rhs.0.x,
            y: self * rhs.0.y,
            z: self * rhs.0.z,
        })
    }
}

impl ops::Mul<Color> for Color {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self {
        Color(Vec3 {
            x: self.0.x * rhs.0.x,
            y: self.0.y * rhs.0.y,
            z: self.0.z * rhs.0.z,
        })
    }
}

impl ops::Add for Color {
    type Output = Self;

    fn add(self, rhs: Self) -> Self {
        Color(Vec3 {
            x: self.0.x + rhs.0.x,
            y: self.0.y + rhs.0.y,
            z: self.0.z + rhs.0.z,
        })
    }
}

impl ops::AddAssign for Color {
    fn add_assign(&mut self, rhs: Self) {
        *self = Color(Vec3 {
            x: self.0.x + rhs.0.x,
            y: self.0.y + rhs.0.y,
            z: self.0.z + rhs.0.z,
        })
    }
}

impl Sum for Color {
    fn sum<I>(iter: I) -> Self
    where
        I: Iterator<Item = Self>,
    {
        iter.fold(Color::new(0, 0, 0), |a, b| a + b)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::Rng;

    #[test]
    fn color_accum() {
        let v = Color(Vec3 {
            x: 0.35,
            y: 0.35,
            z: 0.35,
        });

        let a = (0..7).fold(Color::black(), |acc, c| acc + v);

        assert!((a.0.x - 2.45).abs() < 0.001);
        assert!((a.0.y - 2.45).abs() < 0.001);
        assert!((a.0.z - 2.45).abs() < 0.001);
    }
}
