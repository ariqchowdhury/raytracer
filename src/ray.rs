use rand::Rng;
use std::fmt;
use ultraviolet::Vec3;

#[derive(Copy, Clone)]
pub struct Ray {
    pub origin: Vec3,
    pub direction: Vec3,
    pub t_max: f32,
}

impl Ray {
    pub fn new(o: Vec3, d: Vec3) -> Ray {
        Ray {
            origin: o,
            direction: d,
            t_max: f32::INFINITY,
        }
    }

    pub fn at(&self, t: f32) -> Vec3 {
        self.origin + t * self.direction
    }
}

pub fn random_in_unit_sphere() -> Vec3 {
    loop {
        let p = gen_random_vec3();
        if p.mag_sq() >= 1.0 {
            continue;
        }
        return p;
    }
}

pub fn random_unit_vector() -> Vec3 {
    return random_in_unit_sphere().normalized();
}

pub fn _random_in_hemisphere(n: &Vec3) -> Vec3 {
    let in_unit_sphere = random_in_unit_sphere();
    if in_unit_sphere.dot(*n) > 0.0 {
        return in_unit_sphere;
    } else {
        return -in_unit_sphere;
    }
}

pub fn random_in_unit_disk() -> Vec3 {
    loop {
        let mut p = gen_random_vec3();
        p.z = 0.0;
        if p.mag_sq() >= 1.0 {
            continue;
        } else {
            return p;
        }
    }
}

pub fn gen_random_vec3() -> Vec3 {
    let mut rng = rand::thread_rng();
    Vec3::new(
        rng.gen_range(-1.0..1.0),
        rng.gen_range(-1.0..1.0),
        rng.gen_range(-1.0..1.0),
    )
}

impl fmt::Display for Ray {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "origin:{:?}\ndirection{:?}", self.origin, self.direction)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_mul() {
        let x = Vec3::new(1.0, 2.0, 3.0);
        let y = Vec3::new(2.0, 3.0, 4.0);

        assert_eq!(x * y, Vec3::new(2.0, 6.0, 12.0));

        assert_eq!(y / x, Vec3::new(2.0, 3.0 / 2.0, 4.0 / 3.0));
    }
}
