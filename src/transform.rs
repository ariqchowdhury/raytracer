use crate::bounds::Bounds;
use crate::ray::Ray;
use ultraviolet::{Mat3, Mat4, Vec3, Vec4};

// transformations are represented as 4x4 matrices to perform linear transformations
// on homogenous frames, where a frame is defined by (p, v1, v2, v3);
//
// if s is a scalar, we can represent the point as [s1 s2 s3 1].[v1 v2 v3 p]^T
// the vectors are [s1' s2' s3' 0].[v1 v2 v3 p]^T

pub struct Transform {
    pub m: Mat4,
    pub m_inv: Mat4,
}

impl Transform {
    pub fn id() -> Self {
        Transform {
            m: Mat4::identity(),
            m_inv: Mat4::identity(),
        }
    }

    pub fn new(col1: Vec4, col2: Vec4, col3: Vec4, col4: Vec4) -> Self {
        Transform {
            m: Mat4::new(col1, col2, col3, col4),
            m_inv: Mat4::new(col1, col2, col3, col4).inversed(),
        }
    }

    pub fn new_translate(x: f32, y: f32, z: f32) -> Self {
        Transform::new(
            Vec4::new(1.0, 0.0, 0.0, 0.0),
            Vec4::new(0.0, 1.0, 0.0, 0.0),
            Vec4::new(0.0, 0.0, 1.0, 0.0),
            Vec4::new(x, y, z, 1.0),
        )
    }

    pub fn ray_mul(&self, r: &Ray) -> Ray {
        let o = self.pt_mul(&r.origin);
        let d = self.vec_mul(&r.direction);

        // TODO might need to adjust for error term in FP calculation

        Ray {
            origin: Vec3::new(o.x, o.y, o.z),
            direction: Vec3::new(d.x, d.y, d.z),
            t_max: r.t_max,
        }
    }

    pub fn pt_mul(&self, p: &Vec3) -> Vec4 {
        self.m * Vec4::new(p.x, p.y, p.z, 1.0)
    }

    pub fn pt_mul3(&self, p: &Vec3) -> Vec3 {
        let r = self.pt_mul(p);
        Vec3::new(r.x, r.y, r.z)
    }

    pub fn vec_mul(&self, p: &Vec3) -> Vec4 {
        self.m * Vec4::new(p.x, p.y, p.z, 0.0)
    }

    pub fn norm_mul(&self, n: &Vec3) -> Vec3 {
        let r = self.m_inv.transposed() * Vec4::new(n.x, n.y, n.z, 0.0);
        return r.truncated();
    }

    pub fn bound_mul(&self, b: &Bounds) -> Bounds {
        let p = self.pt_mul3(&Vec3::new(b.p_min.x, b.p_min.y, b.p_min.z));

        let mut ret = Bounds::new_point(&p);

        ret = ret.union_b(&Bounds::new_point(
            &self.pt_mul3(&Vec3::new(b.p_max.x, b.p_min.y, b.p_min.z)),
        ));
        ret = ret.union_b(&Bounds::new_point(
            &self.pt_mul3(&Vec3::new(b.p_min.x, b.p_max.y, b.p_min.z)),
        ));
        ret = ret.union_b(&Bounds::new_point(
            &self.pt_mul3(&Vec3::new(b.p_min.x, b.p_min.y, b.p_max.z)),
        ));
        ret = ret.union_b(&Bounds::new_point(
            &self.pt_mul3(&Vec3::new(b.p_min.x, b.p_max.y, b.p_max.z)),
        ));
        ret = ret.union_b(&Bounds::new_point(
            &self.pt_mul3(&Vec3::new(b.p_max.x, b.p_max.y, b.p_min.z)),
        ));
        ret = ret.union_b(&Bounds::new_point(
            &self.pt_mul3(&Vec3::new(b.p_max.x, b.p_min.y, b.p_max.z)),
        ));
        ret = ret.union_b(&Bounds::new_point(
            &self.pt_mul3(&Vec3::new(b.p_max.x, b.p_max.y, b.p_max.z)),
        ));
        ret
    }

    pub fn swaps_handedness(&self) -> bool {
        let mat3 = Mat3::new(
            self.m[0].truncated(),
            self.m[1].truncated(),
            self.m[2].truncated(),
        );
        mat3.determinant() < 0.0
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_element_access() {
        let i = Transform::new(
            Vec4::new(0.0, 4.0, 8.0, 12.0),
            Vec4::new(1.0, 5.0, 9.0, 13.0),
            Vec4::new(2.0, 6.0, 10.0, 14.0),
            Vec4::new(3.0, 7.0, 11.0, 15.0),
        );

        assert_eq!(i.m[0][0], 0.0);
        assert_eq!(i.m[0][1], 4.0);
        assert_eq!(i.m[0][2], 8.0);
        assert_eq!(i.m[0][3], 12.0);
    }

    #[test]
    fn test_point_translation() {
        let (dx, dy, dz) = (2.0, 4.0, 6.0);
        let t = Transform::new(
            Vec4::new(1.0, 0.0, 0.0, 0.0),
            Vec4::new(0.0, 1.0, 0.0, 0.0),
            Vec4::new(0.0, 0.0, 1.0, 0.0),
            Vec4::new(dx, dy, dz, 1.0),
        );

        let p = Vec3::new(1.0, 3.0, 9.0);
        let q = t.pt_mul(&p);

        assert_eq!(q, Vec4::new(3.0, 7.0, 15.0, 1.0));
    }

    #[test]
    fn test_translation_ignores_vectors() {
        let (dx, dy, dz) = (2.0, 4.0, 6.0);
        let t = Transform::new(
            Vec4::new(1.0, 0.0, 0.0, 0.0),
            Vec4::new(0.0, 1.0, 0.0, 0.0),
            Vec4::new(0.0, 0.0, 1.0, 0.0),
            Vec4::new(dx, dy, dz, 1.0),
        );

        let p = Vec3::new(1.0, 3.0, 9.0);
        let q = t.vec_mul(&p);

        assert_eq!(q, Vec4::new(p.x, p.y, p.z, 0.0));
    }
}
