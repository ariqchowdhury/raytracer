use crate::color::Color;
use ultraviolet::Vec3;

pub trait Texture: Sync + Send {
    fn value(&self, u: f32, v: f32, p: Vec3) -> Color;
}

#[derive(Copy, Clone)]
pub struct SolidColor {
    c: Color,
}

impl SolidColor {
    pub fn new(c: Color) -> SolidColor {
        SolidColor { c: c }
    }
}

impl Texture for SolidColor {
    fn value(&self, u: f32, v: f32, p: Vec3) -> Color {
        return self.c;
    }
}

#[derive(Copy, Clone)]
pub struct Checker<T: Texture> {
    even: T,
    odd: T,
}

impl<T: Texture> Checker<T> {
    pub fn new(even: T, odd: T) -> Checker<T> {
        Checker::<T> {
            even: even,
            odd: odd,
        }
    }
}

impl<T: Texture> Texture for Checker<T> {
    fn value(&self, u: f32, v: f32, p: Vec3) -> Color {
        let sin = (10.0 * p.x).sin() * (10.0 * p.y).sin() * (10.0 * p.z).sin();
        if sin < 0.0 {
            self.even.value(u, v, p)
        } else {
            self.odd.value(u, v, p)
        }
    }
}
